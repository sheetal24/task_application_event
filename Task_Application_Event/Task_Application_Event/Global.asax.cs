﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Task_Application_Event
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            String str = "Application_Start Event Fireed";

            //Response.Write("Application_Start Event Fired");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Response.Write("Session_Start Fired");
            Response.Write("</br>");
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Response.Write("Application_BeginRequest Event Fired");
            Response.Write("</br>");
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            Response.Write("Application_AuthenticateRequest Event Fired");
            Response.Write("</br>");
        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            Response.Write("Session_End Fired");
            Response.Write("</br>");
        }

        protected void Application_End(object sender, EventArgs e)
        {
            Response.Write("Application_End Fired");
            Response.Write("</br>");
        }
    }
}